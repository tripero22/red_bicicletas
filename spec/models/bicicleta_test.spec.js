var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');

describe('Testing Bicicletas', function(){
    beforeAll((done) => { mongoose.connection.close(done) });

    beforeEach( (done) => {
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true });
        mongoose.set('useCreateIndex', true);

        var db = mongoose.connection;
        db.on('error', console.error.bind(console, 'MongoDB connection error: '));
        db.once('open', function () {
            console.log('We are connected to test database!');
            done();
        });
    });

    afterEach( (done) => {
        Bicicleta.deleteMany({}, function (err, success) {
            if (err) console.log(err);

            mongoose.connection.close(done);
        });
    });

    describe('Bicicleta.createInstance', () => {
        it('crea una instancia de Bicicleta', () => {
            var bici = Bicicleta.createInstance(1, "verde", "urbana", [-34.5, -54.1]);

            expect(bici.code).toBe(1);
            expect(bici.color).toBe("verde");
            expect(bici.modelo).toBe("urbana");
            expect(bici.ubicacion[0]).toEqual(-34.5);
            expect(bici.ubicacion[1]).toEqual(-54.1);
        });
    });

    describe('Bicicleta.allBicis', () => {
        it('Comienza vacía', (done) => {
            Bicicleta.allBicis(function (err, bicis) {
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });

    describe('Bicicleta.add', () => {
        it('Agregar una Bicicleta', (done) => {    
            var aBici = new Bicicleta({ code: 1, color: 'rojo', modelo: 'urbana' });

            Bicicleta.add(aBici, function (err, newBici) {
                if (err) console.log(err);
                Bicicleta.allBicis(function (err, bicis) {
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(aBici.code);

                    done();
                });
            });
        });
    });
    
    describe('Bicicleta.findByCode', () => {
        it('debe devolver la bici con code 1', (done) => {    
            Bicicleta.allBicis(function (err, bicis) {
                expect(bicis.length).toEqual(0);

                var aBici = new Bicicleta({ code: 1, color: 'rojo', modelo: 'urbana' });            
                Bicicleta.add(aBici, function (err, newBici) {
                    if (err) console.log(err);
                
                    var aBici2 = new Bicicleta({ code: 2, color: 'azul', modelo: 'ruta'});
                    Bicicleta.add(aBici2, function (err, newBici) {
                        if (err) console.log(err);
                        Bicicleta.findByCode(1, function (err, targetBici) {
                            expect(targetBici.code).toEqual(aBici.code);
                            expect(targetBici.color).toEqual(aBici.color);
                            expect(targetBici.modelo).toEqual(aBici.modelo);

                            done();
                        });
                    });
                });
            });
        });
    });

    describe('Bicicleta.removeByCode', ()=> {
        it('Debe borrar la Bicicleta con codigo 1', (done) => {
            Bicicleta.allBicis(function (err, bicis) {
                expect(bicis.length).toBe(0);

                var abici = new Bicicleta({ code: 1, color: 'azul', modelo: 'montaña' });

                Bicicleta.add(abici, function (err, newBici) {
                    if (err) console.log(err);

                    Bicicleta.removeByCode(1, function (err) {
                        if (err) console.log(err);
                        Bicicleta.allBicis(function (err, newBicis) {
                            expect(newBicis.length).toBe(0);
                            done();
                        });
                    });
                });
            });
        });
    });

});