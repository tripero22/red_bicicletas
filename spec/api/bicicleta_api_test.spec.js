var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');

var baseURL = 'http://localhost:3000/api/bicicletas';

describe('Bicicleta API', () => {
    beforeAll((done) => {
        mongoose.connection.close().then(() => {
            var mongoDB = 'mongodb://localhost/testdb';
            mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true });
            mongoose.set('useCreateIndex', true);
    
            var db = mongoose.connection;
            db.on('error', console.error.bind(console, 'MongoDB connection error: '));
            db.once('open', function () {
                console.log('We are connected to test database!');
                done();
            });
        });
    });

    afterEach( (done) => {
        Bicicleta.deleteMany({}, function (err, success) {
            if (err) console.log(err);

            done();
        });
    });    
       
    
    describe('GET Bicicletas /', () => {
        it('Status 200', (done) => {
            request.get(baseURL, function(err, resp, body){
                var result = JSON.parse(body);
                expect(resp.statusCode).toBe(200);
                expect(result.bicicletas.length).toBe(0);

                done();
            });
        });
    });

    describe('POST Bicicletas /create', () => {
        it('Status 200', (done) => {
            var headers = {'content-type': 'application/json'};
            var bici = '{ "code": 2, "color": "negro", "modelo": "montaña", "lat": 34.60, "lng": -56.481 }';

            request.post({
                headers: headers,
                url: `${ baseURL }/create`,
                body: bici
            }, function(err, resp, body) {
                expect(resp.statusCode).toBe(200);
                
                var aBici = JSON.parse(body).bicicleta;

                expect(aBici.color).toBe('negro');
                expect(aBici.modelo).toBe('montaña');
                expect(aBici.ubicacion[0]).toBe(34.60);
                expect(aBici.ubicacion[1]).toBe(-56.481);

                done();
            });

        });
    });

    describe('UPDATE Bicicletas /update', () => {
        it('Status 200', (done) => {
            var headers = {'content-type': 'application/json'};
            var bici = '{ "code": 3, "color": "azul", "modelo": "urbana", "lat": 3.476, "lng": -76.488 }';

            var aBici = new Bicicleta({ code: 3, color: 'verde', modelo: 'montaña', ubicacion: [-54.468,-56.488]});
        
            Bicicleta.add(aBici, function (err, newBici) {
                request.put({
                    headers: headers,
                    url: `${ baseURL }/update`,
                    body: bici
                }, function(err, resp, body) {
                    expect(resp.statusCode).toBe(200);
                    
                    Bicicleta.findByCode(aBici.code, function (err, targetBici) {

                        expect(targetBici.color).toBe('azul');
                        expect(targetBici.modelo).toBe('urbana');
                        expect(targetBici.ubicacion[0]).toBe(3.476);
                        expect(targetBici.ubicacion[1]).toBe(-76.488);
                        
                        done();
                    });
                }); 
            });
        });
    });

    describe('DELETE Bicicletas /delete', () => {
        it('Status 204', (done) => {
            var aBici = Bicicleta.createInstance(4, 'verde', 'ruta', [-54.468,-56.488]);
            
            Bicicleta.add(aBici, function (err, newBici) {
                var headers = {'content-type': 'application/json'};
                var bici = '{ "code": 4 }';
                
                request.delete({
                    headers: headers,
                    url: `${ baseURL }/delete`,
                    body: bici
                }, function(err, resp, body) {
                    expect(resp.statusCode).toBe(204);
                    
                    Bicicleta.allBicis(function (err, newBicis) {
                        expect(newBicis.length).toBe(0);

                        done();
                    });
                });
            });
        });
    });
});